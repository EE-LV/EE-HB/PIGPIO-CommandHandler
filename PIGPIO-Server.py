#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A simple command handler listens to incoming TCPI/IP connection on Raspi.

It handles requests to read GPIO status or to set GPOut bits.

Created on Wed Jan 16 12:38:57 2019.

@author: H.Brandgsi.de
"""

import json
import os
import re
import socketserver
import sys
import RPi.GPIO as GPIO
from argparse import ArgumentParser

# Global variable neccessary because socketserver.BaseRequestHandler is abstract class?
configuration_filename = "Settings.json"


class PIGPIORequestHandler(socketserver.BaseRequestHandler):
    """
    Handles incoming connections and executes commands.

    Read and set GPIO on Raspberry PI.
    Command descriptions can be requested using '*HELP?'
    """

    addr = None
    input_pins = None
    output_pins = None
    local_path = ""
    path_separator = "/"
    settings_file_name = ""
    settings_file_path = ""

    def setup(self):
        """Set up GPIO using configuration from JSON."""
        if sys.platform == "win32":
            self.path_separator = "\\"
        self.local_path = os.path.abspath(".")
        self.settings_file_name = configuration_filename
        self.settings_file_path = self.local_path + self.path_separator \
            + self.settings_file_name
        print("Using:", self.settings_file_path)
        try:
            with open(self.settings_file_path, "r") as settings_file:
                settings = json.load(settings_file)
                self.input_pins = settings["Input"]
                self.output_pins = settings["Output"]
        except FileNotFoundError as execption:
            print(execption)
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        for i in self.input_pins.values():
            GPIO.setup(i, GPIO.IN)
        for i in self.output_pins.values():
            GPIO.setup(i, GPIO.OUT, initial=0)
        self.addr = self.client_address[0]
        print("[{}] Verbindung hergestellt".format(self.addr))

    def handle(self):
        """Handle received commands."""
        while True:
            received = self.request.recv(1024)
            if received:
                print("[{}] {}".format(self.addr, received.decode()))
                command = received.decode()
                if command == "*IDN?":
                    response = "PIGPIO-Server V1.1.0.0"
                elif command == "*HELP?":
                    response = "Help for PIGPIO-Server:\n"\
                        + "*IDN?:\tIdentification query incl. FW-Revision.\n"\
                        + "*HELP?:\tThis help text.\n"\
                        + "*RST:\tReset all GPO to false.\n"\
                        + "*SYM?:\tReturn all symbols.\n"\
                        + "*TST?:\tPerform selftest.\n"\
                        + "ALL 0|1:\tSet all GPO to False|True.\n"\
                        + "GPO n 0|1:\tSet GPO pin n to False|True.\n"\
                        + "SYM name 0|1:\tSet GPO pin assigned to name to False|True.\n"\
                        + "STATUS?:\tRead status of all GPIO."
                elif command.upper() == "QUIT":
                    response = "Client quit."
                    break
                elif command == "*RST":
                    response = "Reset GPO to False"
                    for k in self.output_pins.values():
                        GPIO.output(k, False)
                elif command == "*SYM?":
                    response = "Input pins: " + str(self.input_pins) + "\n"\
                        + "Output pins: " + str(self.output_pins)
                elif command == "*TST?":
                    response = "Selftest OK"
                elif re.match(r"ALL", command):
                    command_parameter = re.split(r"\s", command)
                    state = int(command_parameter[1]) > 0
                    response = "ALL " + str(state)
                    for k in self.output_pins.values():
                        GPIO.output(k, state)
                elif re.match(r"GPO", command):
                    command_parameter = re.split(r"\s", command)
                    pin = int(command_parameter[1])
                    if pin in self.output_pins.values():
                        state = int(command_parameter[2]) > 0
                        response = "GPO " + str(pin) + " " + str(state)
                        GPIO.output(pin, state)
                    else:
                        response = "Error-Unknown Channel: Channel "\
                            + str(pin)\
                            + " is not available."
                elif re.match(r"SYM", command):
                    command_parameter = re.split(r"\s", command)
                    symbol = command_parameter[1]
                    if symbol in self.output_pins:
                        state = int(command_parameter[2]) > 0
                        response = "SYM " + str(symbol) + " " + str(state)
                        GPIO.output(self.output_pins[symbol], state)
                    else:
                        response = "Error-Unknown symbol: "\
                            + symbol \
                            + " is not available."
                elif command == "STATUS?":
                    response = "Input: "
                    for pin in self.input_pins.values():
                        status = GPIO.input(pin)
                        response += str(pin) + "=" + str(status) + " "
                    response += "\nOutput: "
                    for pin in self.output_pins.values():
                        status = GPIO.input(pin)
                        response += str(pin) + "=" + str(status) + " "
                else:
                    response = "Error-Unknown command:"
                    response += received.decode()
                print(response)
                response += "EOT\n"
                self.request.sendall(response.encode())

    def finish(self):
        """Set all outputs to false and cleanup when connection is closed."""
        print("[{}] Verbindung geschlossen".format(self.addr))
        for k in self.output_pins.values():
            GPIO.output(k, False)
            response = "All Off"
        print(response)
        GPIO.cleanup()
        print("GPIO.cleanup done.")


if __name__ == "__main__":
    # execute only if run as a script
    print("Operation system is " + sys.platform)
    print("Local path = ", os.path.abspath("."))
    parser = ArgumentParser()
    parser.add_argument("-c", "--configuration", dest="configuration_file", default="Settings.json")
    parser.add_argument("-p", "--port", dest="port_number", default=50000)
    args = parser.parse_args()
    print("Configuration file:", args.configuration_file)
    configuration_filename = args.configuration_file
    print("Starting request handler...")
    port = int(args.port_number)
    port_max = port + 10
    server = None
    while server is None and port < port_max:
        try:
            server = socketserver.ThreadingTCPServer(
                ("", port),
                PIGPIORequestHandler
            )
            print("Server listening to port " + str(port))
            # server.serve_forever()
            while True:
                server.handle_request()
        except OSError:
            port += 1
            continue
