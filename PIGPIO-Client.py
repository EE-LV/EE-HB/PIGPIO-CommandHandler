#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Client connecting to PIGPIO-Server running on a Raspberry PI.

Created on Wed Jan 16 12:38:57 2019*

@author: H.Brandgsi.de
"""
import socket

port = 50000
ip = input("IP-Adresse: ")
# ip = "140.181.71.137"
# ip = "192.168.178.45"
# ip = "localhost"
print("PIGPIO-Client V1.0.0.0")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True and port < 60000:
    try:
        s.connect((ip, port))
        print("Connected to port ", port)
        print("Type '*HELP?' for available commands.\n")
        try:
            while True:
                nachricht = input("Nachricht (<CTRL>+D:Quit): ")
                s.send(nachricht.encode())
                response = s.recv(1024)
                print("{}".format(response.decode()))
                if nachricht.upper() == "QUIT":
                    break
        finally:
            print("Disconnected.")
            s.close()
            pass
    except BaseException:
        port += 1
