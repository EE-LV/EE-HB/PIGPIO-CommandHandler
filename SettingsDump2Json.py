"""
Created on Wed Jan 16 12:38:57 2019
Script to be used to evalue Python.
@author: H.Brandgsi.de
"""
import os
import json

LocalPath = os.path.abspath(".")
print("Localpath:", LocalPath)
FilePath = LocalPath + "\\Setting-Test.json"
print("Filepath:", FilePath, "exists: ", os.path.exists(FilePath))

Settings = {"Input" : {\
                       "Channel_0" : 23
                       },\
            "Output" : {\
                        "Channel_0" : 17,\
                        "Channel_1" : 27,\
                        "Channel_2" : 22,\
                        "Channel_3" : 18\
                        }                    
            }
#print("JSON-Seetings:", json.dumps(Settings))
with open(FilePath, "w") as SettingsFile:
    json.dump(Settings, SettingsFile)