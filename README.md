PIGPIO-CommandHandler README
===========
This project is used to evaluate Python and Raspberry PI GPIO.

A simple command handler listens to incoming TCPI/IP connection and handles requests
to read GPIO status or to set GPOut bits.

Currently used development SW is Pythjon 3.7 & LabVIEW 2018

PIGPIO-CommandServer
--------------------
Launch server script on PI:
>python3 PIGPIO-CommandServer.py

Client computer
---------------
Launch client script:
>python3 PIGPIO-Client.py #You will be ask for PIs IP.

Alternatively you can use the LabVIEW instrument driver to connect the PIGPIO-CommandServer.
It's available in the LabVIEW folder. A LV2013 version is available in the corresponding folder.

Related documents and information
=================================
- README.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/EE-HB/PIGPIO-CommandHandler

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2019  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.