#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test client connecting to PIGPIO-Server running on a Raspberry PI.

Created on Wed Jan 16 12:38:57 2019*

@author: H.Brandgsi.de
"""

import sys
from argparse import ArgumentParser

if __name__ == "__main__":
    # execute only if run as a script
    print("Operation system is " + sys.platform)
    # print(sys.argv)
    parser = ArgumentParser()
    parser.add_argument("-c", "--configuration", dest="configuration_file", default="Settings.json")
    args = parser.parse_args()
    if args.configuration_file:
        print("Configuration file:", args.configuration_file)
    else:
        parser.error("{} is not set.".format(args.configuration_file))
