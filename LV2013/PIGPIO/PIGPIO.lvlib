﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="13008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2018_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2018\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2018_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2018\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver as client interface for the PIGPIO_CommandServer on PI with can switch Outputs and report the actual status of input and output bits.

Lizenziert unter EUPL V. 1.1

Copyright 2019 GSI Helmholtzzentrum für Schwerionenforschung GmbH
H.Brand@gsi.de, EEL, Planckstr. 1, 64291 Darmstadt, Germany
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&amp;!!!*Q(C=\:1R4E*"%):`D95N.R#O-&amp;@A#FRBLE"LI1G6)3;;4/)*O!+6`6S"4FN+7PT?PHGIB&gt;"IIIG\$,T^:X&lt;G?\P,3KV&gt;3B@;HWK\I[XTN[$^LH]9"D6R'/UK:"A=BB`G&gt;Z/(Q3@`96\P_-L@VR_#X_M@Z^@2^O``^`^N`YF'T08*I"^OUFEDR2),T$&amp;L&lt;\&gt;0^%20^%20^%10^%!0^%!0^%"X&gt;%&gt;X&gt;%&gt;X&gt;%=X&gt;%-X&gt;%-X&gt;%.P(6XI1B=[KZ)54QIF3:-%34!I3LY3HI1HY5FY'#LB38A3HI1HY3&amp;%#5`#E`!E0!E0UZ4Q*$Q*4]+4]*#KE74L[0!E0+28Q"0Q"$Q"4]"$315]!5"1,%A=*!&amp;$A4.Y#(A#HI#(2Q5]!5`!%`!%0,A6]!1]!5`!%`!QJ;V+.*KOI].$'DE]$I`$Y`!Y0+37Q_0Q/$Q/D].$/4E]$I]$Y22UEI-A:Z)4Y!Q=(I?((TE]$I`$Y`!Y0,D;$HF&lt;G9[G[_DQ'$Q'D]&amp;D]"A]J*$"9`!90!;0Q5.;'4Q'D]&amp;D]"A]F*,"9`!90!;)5:4S-J):%YUA1T"Y_,449GW8IJ&amp;9[`88\!_K[A#K$J&lt;KQ+A/AGK$62OHWB$61KM75,5QKB&gt;7P9A+5&amp;69F6!6K#X@'WS.L&lt;!&amp;.M&gt;GW"3&lt;9+.O[D=(&lt;L&gt;&lt;&lt;49&lt;L&gt;&gt;LL69L,29,T?&gt;TT79T4;&gt;44394D5;DQWVV2?`&lt;W8!P8@0]&gt;0@S@,^]'$]^PI[8^\@DZ&gt;X.=[@`J08XUG_Y'X7O`9&gt;LHD6[!TT;SZA!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
		<Item Name="Transaction.vi" Type="VI" URL="../Private/Transaction.vi"/>
		<Item Name="Symbol Str to ClusterArray.vi" Type="VI" URL="../Private/Symbol Str to ClusterArray.vi"/>
		<Item Name="Status Str to ClusterArray.vi" Type="VI" URL="../Private/Status Str to ClusterArray.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="GPO All.vi" Type="VI" URL="../Public/Action-Status/GPO All.vi"/>
			<Item Name="Symbols.vi" Type="VI" URL="../Public/Action-Status/Symbols.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="GPO Pin.vi" Type="VI" URL="../Public/Configure/GPO Pin.vi"/>
			<Item Name="GPO Pins.vi" Type="VI" URL="../Public/Configure/GPO Pins.vi"/>
			<Item Name="GPO Symbol.vi" Type="VI" URL="../Public/Configure/GPO Symbol.vi"/>
			<Item Name="GPO Symbols.vi" Type="VI" URL="../Public/Configure/GPO Symbols.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="GPIO.vi" Type="VI" URL="../Public/Data/GPIO.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
			<Item Name="Self-Test.vi" Type="VI" URL="../Public/Utility/Self-Test.vi"/>
			<Item Name="Help.vi" Type="VI" URL="../Public/Utility/Help.vi"/>
		</Item>
		<Item Name="Example.vi" Type="VI" URL="../Public/Example.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="PIGPIO Readme.html" Type="Document" URL="/&lt;instrlib&gt;/PIGPIO/PIGPIO Readme.html"/>
</Library>
