"""
Created on Wed Jan 16 12:38:57 2019
Script to be used to evalue Python.
@author: H.Brandgsi.de
"""
import os
import json
import sys

if sys.platform == "win32":
    path_separator = "\\"
else:
    path_separator = "/"
print("Operation system is " + sys.platform)

local_path = os.path.abspath(".")
settings_file_path = local_path + path_separator + "Settings-Test.json"

with open(settings_file_path, "r") as settings_file:
    settings = json.load(settings_file)
    input_pins = settings["Input"]
    output_pins = settings["Output"]
    #IChnName = "Kopplungsmagnete" 
    #print("ID of " + IChnName +":"+ str(Input[IChnName]))
    inp = input_pins.values()
    out = output_pins.values()
    all_pins = input_pins
    all_pins.update(output_pins)



